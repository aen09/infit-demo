import React, { Component } from "react";
import {
  View,
  Container,
  Content,
  Header,
  Left,
  Button,
  Icon,
  Title,
  Text
} from "native-base";
import SessionList from "../components/listComponents/SessionList";
import { Actions } from "react-native-router-flux";

// import { Title } from "react-native-paper";
// import Header from "../components/basicStyleComponents/Header";

export default class componentName extends Component {
  render() {
    return (
      <Container style={{ backgroundColor: "#f9f9f9" }}>
        <Header style={{ backgroundColor: "#fdc000" }}>
          <Left style={{ flex: 1 }}>
            <Button
              style={{ elevation: 0, backgroundColor: "transparent" }}
              onPress={() => Actions.pop()}
            >
              <Icon
                name="ios-arrow-round-back"
                style={{ fontSize: 32, color: "white", marginRight: 10 }}
              />
              <Title
                style={{
                  fontSize: 16,
                  color: "white",
                  marginLeft: 10,
                  fontWeight: "600"
                }}
              >
                Session List
              </Title>
            </Button>
          </Left>
        </Header>
        <Content>
          {this.props.data.length === 0 ? (
            <Title>Title</Title>
          ) : (
            <SessionList data={this.props.data} />
          )}
        </Content>
      </Container>
    );
  }
}
