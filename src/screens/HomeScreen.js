import React, { Component } from "react";
import {
  View,
  Text,
  Alert,
  AsyncStorage,
  Image,
  NetInfo,
  FlatList
} from "react-native";
import { ToastAndroid, BackHandler } from "react-native";
// import BackgroundTask from "react-native-background-task";
import BackgroundGeolocation from "@mauron85/react-native-background-geolocation";
// import firebase from "react-native-firebase";
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";

import Toast, { DURATION } from "react-native-easy-toast";

import geolib from "geolib";
import moment from "moment";
import { Avatar, Card, Title, Paragraph } from "react-native-paper";
import { get } from "lodash";
import {
  Container,
  Content,
  Tab,
  Tabs,
  Footer,
  FooterTab,
  Button,
  Right,
  Body,
  Left
} from "native-base";
import { Actions } from "react-native-router-flux";
import Headers from "../components/homeScreenComponents/Header";
import SessionList from "../components/homeScreenComponents/SessionList";
import CurrentSessionTab from "../components/homeScreenComponents/CurrentSessionTab";
import NoInternetScreen from "../components/CommonCompponent/NoInternetScreen";
// import { nullLiteral } from "@babel/types";
const URL = "https://api.infit.apps.actonatepanel.com/graphql";

const createTest = `mutation createTest($input : test_demo_locationInputType!) {
  test_demo_location{
    database{
      create(input : $input) {
        _id
        user_id
        start_time
        end_time
        date
        latitude
        longitude
        _deleted
        _created_at
        _updated_at
      }
    }
  }
}`;

const getDataLocation = `
query getDataLocation( $user_id: String) {
  test_demo_location{
    database{
      all(find:{
        user_id: $user_id
      }){
        _id
        user_id
        start_time
        end_time
        date
        latitude
        longitude
      }
    }
  }
}
`;

const fixedMeter = 30;

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      latitude: null,
      longitude: null,
      timeoutSession: null,
      gymLatitude: 22.3370058,
      gymLongitude: 73.2029943,
      currentSessionStatus: null,
      currentSessionDetails: null,
      sessionList: [],
      noInternet: false,
      currentSessionTime: "",
      sessionStartime: null,
      isShowingText: true,
      warningTimeout: "",
      checkOutTime: "",
      warning: false,
      currentLocation: null
    };
    setInterval(
      () =>
        this.setState(previousState => ({
          isShowingText: !previousState.isShowingText
        })),
      1000
    );

    // this.ref = firebase.firestore().collection("SessionData");
  }

  showToast(message) {
    this.refs.toast.show(message, 500, () => {
      // something you want to do at close
    });
  }
  // componentDidMount() {
  //   // this.enableLocation();
  //   this.getCurrentLocation();
  //   this.intialGetData();
  //   this.setGymLocation();
  //   this.configBgLocation();

  //   // DeviceEventEmitter.addListener("locationProviderStatusChange", function(
  //   //   status
  //   // ) {
  //   //   // only trigger when "providerListener" is enabled
  //   //   console.log("shoaib", status); //  status => {enabled: false, status: "disabled"} or {enabled: true, status: "enabled"}
  //   //   if (!status.enabled) {
  //   //     this.enableLocation();
  //   //   }
  //   // });
  // }

  // enableLocation() {
  //   // console.log("kkkk");
  //   LocationServicesDialogBox.checkLocationServicesIsEnabled({
  //     message: "<font color='#f1eb0a'>Use Location ?</font>",
  //     ok: "YES",
  //     cancel: "NO",
  //     style: {
  //       // (optional)
  //       backgroundColor: "#87a9ea", // (optional)

  //       positiveButtonTextColor: "#ffffff", // (optional)
  //       positiveButtonBackgroundColor: "#5fba7d", // (optional)

  //       negativeButtonTextColor: "#ffffff", // (optional)
  //       negativeButtonBackgroundColor: "#ba5f5f" // (optional)
  //     }
  //   })
  //     .then(function(success) {
  //       console.log(success);
  //     })
  //     .catch(error => {
  //       console.log(error.message);
  //       // if (error.message === "disable") {
  //       //   // this.enableLocation();
  //       // }
  //     });
  // }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    // this.goBack(); // works best when the goBack is async
    BackHandler.exitApp();
    return true;
  };

  componentWillUnmount() {
    NetInfo.removeEventListener(
      "connectionChange",
      handleFirstConnectivityChange
    );
  }
  componentDidMount() {
    console.log("BackgroundGeolocation", BackgroundGeolocation);
    console.disableYellowBox = true;
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);

    NetInfo.addEventListener(
      "connectionChange",
      handleFirstConnectivityChange => {
        console.log("connectionChange", handleFirstConnectivityChange);
        if (handleFirstConnectivityChange.type === "none") {
          this.setState({
            noInternet: true
          });
        } else {
          console.log("hi i am no on ");

          this.setState({
            noInternet: false
          });
        }
      }
    );

    this.getCurrentLocation();
    this.intialGetData();
    this.setGymLocation();
    this.configBgLocation();
  }

  async intialGetData() {
    const sessionStatus = await AsyncStorage.getItem("SESSION_STATUS");
    const timeoutTime = await AsyncStorage.getItem("TIMEOUT_TIME");
    const sessionDetails = await AsyncStorage.getItem("SESSION_DETAILS");
    const sessionStartime = await AsyncStorage.getItem("SESSION_START_TIME");
    console.log("hoho", sessionStartime);

    if (timeoutTime) {
      this.setState({
        warningTimeout: timeoutTime
      });
    }

    const session = JSON.parse(sessionDetails);
    if (sessionStatus === "start" && session) {
      console.log("-->", sessionStatus, JSON.parse(sessionDetails));
      const sessionTime = this.getMinitue(session._startTime, new Date());
      this.setState({
        currentSessionStatus: sessionStatus,
        currentSessionDetails: session,
        sessionStartime: sessionStartime
      });
      console.log("sessionTime", sessionTime, warningTimeout);
      const distance = this.getDistance();
      const checkOutTimeSession = this.getSessionTime(warningTimeout);
      console.log("checkOutTimeSession", checkOutTimeSession);

      if (
        sessionTime > 45 &&
        distance > fixedMeter &&
        checkOutTimeSession > 5
      ) {
        this.onLostSession();
      }
    }
  }

  async setGymLocation() {
    const userDetails = await AsyncStorage.getItem("USERDETAILS");
    if (userDetails) {
      const user = JSON.parse(userDetails);
      this.setState({ user });
      if (user) {
        this.getDataFromDb(user._id);
      }
      if (user.lat_lon) {
        let x = user.lat_lon;
        let strParts = x.split(",");
        console.log(
          "this is location",
          parseFloat(strParts[0]),
          parseFloat(strParts[1])
        );
        this.setState({
          gymLatitude: parseFloat(strParts[0]),
          gymLongitude: parseFloat(strParts[1])
        });
      }
    }
  }

  async getCheckoutTime() {
    const timeoutTime = await AsyncStorage.getItem("TIMEOUT_TIME");

    return timeoutTime;
  }

  checkStatus() {
    if (
      this.state.currentSessionDetails &&
      this.state.currentSessionDetails._startTime
    ) {
      return true;
    }
    return false;
  }

  onAuthrization() {
    BackgroundGeolocation.on("authorization", status => {
      console.log(
        "[INFO] BackgroundGeolocation authorization status: " + status
      );
      if (status !== BackgroundGeolocation.AUTHORIZED) {
        // we need to set delay or otherwise alert may not be shown
        setTimeout(
          () =>
            Alert.alert(
              "App requires location tracking permission",
              "Would you like to open app settings?",
              [
                {
                  text: "Yes",
                  onPress: () => BackgroundGeolocation.showAppSettings()
                },
                {
                  text: "No",
                  onPress: () => console.log("No Pressed"),
                  style: "cancel"
                }
              ]
            ),
          1000
        );
      }
    });
  }

  getSessionTime(startDate) {
    if (!startDate) {
      return null;
    }
    const date = new Date();
    let timeremaininig = date.getTime() - new Date(startDate).getTime();

    // console.log("date===>234", new Date(startDate._startTime).getTime());
    var minutes = Math.floor(timeremaininig / 60000);
    // var seconds = ((timeremaininig % 60000) / 1000).toFixed(0);

    return minutes;
  }

  configBgLocation() {
    BackgroundGeolocation.configure({
      desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
      stationaryRadius: 50,
      distanceFilter: 50,
      notificationTitle: "Infit",
      notificationText: "enabled",
      debug: false,
      startOnBoot: true,
      stopOnTerminate: false,
      locationProvider: BackgroundGeolocation.ACTIVITY_PROVIDER,
      interval: 1000,
      fastestInterval: 5000,
      activitiesInterval: 1000,
      stopOnStillActivity: false,
      url: "http://192.168.81.15:3000/location",
      httpHeaders: {
        "X-FOO": "bar"
      },
      // customize post properties
      postTemplate: {
        lat: "@latitude",
        lon: "@longitude",
        foo: "bar" // you can also add your own properties
      }
    });
    BackgroundGeolocation.start();

    this.onAuthrization();

    BackgroundGeolocation.on("location", async location => {
      if (location && location.latitude && location.longitude) {
        this.setState({
          currentLocation: location,
          latitude: location.latitude,
          longitude: location.longitude
        });
      }

      const distance = this.getDistance();
      console.log("distance in  BackgroundGeolocation.on location", distance);
      const ongoingSessionStatus = this.checkStatus();

      if (ongoingSessionStatus) {
        const { currentSessionDetails } = this.state;
        const timeoutSession = await AsyncStorage.getItem("TIMEOUT_TIME");
        const sessionTime = this.getSessionTime(
          currentSessionDetails._startTime
        );
        const checkOutTimeSession = this.getSessionTime(timeoutSession);

        console.log(
          "checkOutTimeSession in  BackgroundGeolocation.on location",
          checkOutTimeSession,
          timeoutSession
        );
        console.log(
          "sessionTime in  BackgroundGeolocation.on location",
          sessionTime
        );

        if (distance > fixedMeter && timeoutSession === null) {
          console.log("i am in warning");
          this.setState({ warning: true });
          AsyncStorage.setItem("TIMEOUT_TIME", new Date().toString());
        }
        if (
          sessionTime > 44 &&
          distance > fixedMeter &&
          checkOutTimeSession > 5
        ) {
          this.showToast("Session Declined.");
          // ToastAndroid.show("Session Declined.", ToastAndroid.LONG);
          this.onLostSession();
          return;
        }
        if (
          distance < fixedMeter &&
          timeoutSession &&
          checkOutTimeSession < 5
        ) {
          this.setState({
            timeoutSession: null
          });
          timeoutSession = null;
          AsyncStorage.removeItem("TIMEOUT_TIME");
        }
        if (timeoutSession) {
          this.setState({
            timeoutSession: checkOutTimeSession
          });
        }

        console.log("opo", distance, timeoutSession, checkOutTimeSession);
        if (
          distance > fixedMeter &&
          checkOutTimeSession &&
          checkOutTimeSession > 5
        ) {
          console.log("declined");
          this.showToast("Session Declined.");
          // ToastAndroid.show("Session Declined.", ToastAndroid.LONG);
          this.onLostSession();
        }
      }
    });

    // BackgroundGeolocation.on("location", async location => {
    //   console.log("locationlocation", location);

    //   if (location && location.latitude && location.longitude) {
    //     this.setState({
    //       currentLocation: location,
    //       latitude: location.latitude,
    //       longitude: location.longitude
    //     });
    //   }

    //   const distance = this.getDistance();
    //   console.log("sessionStartime", this.state);
    //   if (
    //     this.state.currentSessionDetails &&
    //     this.state.currentSessionDetails._startTime
    //   ) {
    //     console.log("onLocation sessionTime 11", sessionTime);
    //     const timeOut = await AsyncStorage.getItem("TIMEOUT_TIME");
    //     const sessionTime = this.getMinitue(
    //       this.state.currentSessionDetails._startTime,
    //       new Date()
    //     );
    //     console.log("onLocation sessionTime", sessionTime);

    //     this.setState({
    //       currentSessionTime: sessionTime
    //     });
    //     if (sessionTime > 44) {
    //       this.stopSession();
    //     }

    //     if (distance > 100 && !this.state.warning) {
    //       console.log("ok i am TIMEOUT_TIME");
    //       this.setState({ warning: true });
    //       AsyncStorage.setItem("TIMEOUT_TIME", new Date().toString());
    //     }
    //     const checkOutTime = this.getMinitue(timeOut, new Date());
    //     if (distance < 100 && this.state.warning && checkOutTime < 5) {
    //       AsyncStorage.removeItem("TIMEOUT_TIME");
    //     }

    //     if (timeOut && checkOutTime && checkOutTime < 5 && distance > 100) {
    //       this.setState({
    //         checkOutTime
    //       });
    //     }

    //     this.setState({
    //       currentSessionTime: sessionTime
    //     });

    //     if (distance > 1 && checkOutTime > 1 && timeOut) {
    //       console.log("i am checkout", checkOutTime);
    //       ToastAndroid.show("Session Declined.", ToastAndroid.LONG);
    //       this.setState({
    //         sessionStartime: null,
    //         currentSessionStatus: null,
    //         currentSessionDetails: null,
    //         currentSessionTime: null
    //       });
    //       AsyncStorage.removeItem("TIMEOUT_TIME");
    //       AsyncStorage.removeItem("SESSION_START_TIME");
    //       AsyncStorage.removeItem("SESSION_STATUS");
    //       AsyncStorage.removeItem("SESSION_DETAILS");
    //     }
    //   }

    //   // if (distance < 100 && this.state.sessionStartime) {
    //   //   console.log("cvcx", distance, checkOutTime, timeOut);

    //   //   // if (distance > 100 && checkOutTime > 3) {
    //   //   //   ToastAndroid.show("You are not at Gym.", ToastAndroid.LONG);
    //   //   // }
    //   // }

    //   BackgroundGeolocation.startTask(taskKey => {
    //     // execute long running task
    //     console.log("location startTask", taskKey);

    //     BackgroundGeolocation.endTask(taskKey);
    //   });
    // });

    BackgroundGeolocation.on("stationary", stationaryLocation => {
      // handle stationary locations here
      console.log("stationaryLocation", stationaryLocation);
      // Actions.sendLocation(stationaryLocation);
    });

    BackgroundGeolocation.on("error", error => {
      console.log("[ERROR] BackgroundGeolocation error:", error);
    });

    BackgroundGeolocation.on("background", () => {
      console.log("[ERROR] BackgroundGeolocation background:");

      BackgroundGeolocation.start();
    });
    BackgroundGeolocation.on("foreground", () => {
      console.log("[ERROR] BackgroundGeolocation foreground:");

      BackgroundGeolocation.start();
    });

    BackgroundGeolocation.on("start", () => {
      console.log("[INFO] BackgroundGeolocation service has been started");
    });

    // BackgroundGeolocation.on("stop", () => {
    //   console.log("[INFO] BackgroundGeolocation service has been stopped");
    // });

    // BackgroundGeolocation.on("authorization", status => {
    //   console.log(
    //     "[INFO] BackgroundGeolocation authorization status: " + status
    //   );
    //   if (status !== BackgroundGeolocation.AUTHORIZED) {
    //     // we need to set delay or otherwise alert may not be shown
    //     setTimeout(
    //       () =>
    //         Alert.alert(
    //           "App requires location tracking permission",
    //           "Would you like to open app settings?",
    //           [
    //             {
    //               text: "Yes",
    //               onPress: () => BackgroundGeolocation.showAppSettings()
    //             },
    //             {
    //               text: "No",
    //               onPress: () => console.log("No Pressed"),
    //               style: "cancel"
    //             }
    //           ]
    //         ),
    //       1000
    //     );
    //   }
    // });

    // BackgroundGeolocation.on("abort_requested", () => {
    //   console.log("[INFO] Server responded with 285 Updates Not Required");
    //   // Here we can decide whether we want stop the updates or not.
    //   // If you've configured the server to return 285, then it means the server does not require further update.
    //   // So the normal thing to do here would be to `BackgroundGeolocation.stop()`.
    //   // But you might be counting on it to receive location updates in the UI, so you could just reconfigure and set `url` to null.
    // });
    BackgroundGeolocation.on("http_authorization", () => {
      console.log("[INFO] App needs to authorize the http requests");
    });

    BackgroundGeolocation.checkStatus(status => {
      try {
        BackgroundGeolocation.start(); //triggers start on start event
      } catch (err) {
        console.log("yeh error 1 ", err);
      }
      console.log(
        "[INFO] BackgroundGeolocation service is running",
        status.isRunning
      );
      console.log(
        "[INFO] BackgroundGeolocation services enabled",
        status.locationServicesEnabled
      );
      console.log(
        "[INFO] BackgroundGeolocation auth status: " + status.authorization
      );
      // you don't need to check status before start (this is just the example)
      if (!status.isRunning) {
        try {
          BackgroundGeolocation.start(); //triggers start on start event
        } catch (err) {
          console.log("BackgroundGeolocation err", err);
        }
      }
    });

    // you can also just start without checking for status
    // BackgroundGeolocation.start();
  }

  getCurrentLocation() {
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setLocation(position);
      },
      error => this.setState({ error: error.message }),
      { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 }
    );
  }

  setLocation(position) {
    this.setState({
      currentLocation: position.coords,
      latitude: position.coords.latitude,
      longitude: position.coords.longitude,
      error: null
    });
  }

  // getDistancefromGym() {
  //   const { gymLatitude, gymLongitude, latitude, longitude } = this.state;
  //   if (!gymLatitude && !gymLongitude && !latitude && !longitude) {
  //     return "set location";
  //   }

  //   const distance = geolib.getDistance(
  //     { latitude: gymLatitude, longitude: gymLongitude },
  //     { latitude: latitude, longitude: longitude }
  //   );
  //   return distance;
  // }

  getDistance() {
    // if (this.state.sessionList && this.state.sessionList.length === 0) {
    //   this.getDataFromDb(this.state.user ? this.state.user._id : "");
    // }
    if (
      this.state.latitude &&
      this.state.longitude &&
      this.state.gymLatitude &&
      this.state.gymLongitude
    ) {
      const distance = geolib.getDistance(
        {
          latitude: this.state.gymLatitude,
          longitude: this.state.gymLongitude
        },
        { latitude: this.state.latitude, longitude: this.state.longitude }
      );
      return distance;
    }

    return false;
  }

  onCheckStartSession(distance) {
    if (this.state.currentSessionStatus === "start") {
      console.log("currentSessionStatus", this.state.currentSessionStatus);
      return false;
    }
    if (distance < fixedMeter) {
      return true;
    } else {
      return false;
    }
    return false;
  }

  onCheckTodaySession(list) {
    const { user, sessionList } = this.state;
    this.getDataFromDb(user._id);
    const index = list.findIndex(item => {
      if (
        moment(item.start_time).format("DD/MM/YYYY") ===
        moment().format("DD/MM/YYYY")
      ) {
        return true;
      }
      return false;
    });
    if (index > -1) {
      return true;
    }
    return false;
  }

  startSession() {
    const { latitude, longitude } = this.state;
    const distanceFromGym = this.getDistance();

    if (distanceFromGym > fixedMeter) {
      this.showToast("Please start session in gym.");
      // ToastAndroid.show("Please start session in gym.", ToastAndroid.LONG);
      return;
    }

    // if (distanceFromGym > 50) {
    //   ToastAndroid.show(
    //     "You are not at gym. start session after going to gym.",
    //     ToastAndroid.LONG
    //   );
    //   return;
    // }
    const { user, sessionList } = this.state;
    const temp = this.getDataFromDb(user._id);
    const isCheckCompleteToday = this.onCheckTodaySession(sessionList);
    console.log(
      "isCheckCompleteToday",
      isCheckCompleteToday,
      this.onCheckStartSession(distanceFromGym)
    );

    if (isCheckCompleteToday) {
      this.showToast(
        "You have logged your sessions for today, try again tomorrow."
      );
      // ToastAndroid.show(
      // ,
      //   ToastAndroid.LONG
      // );
      return;
      // console.log("hello 0--0-0-0>", isCheckCompleteToday);
    }

    if (this.onCheckStartSession(distanceFromGym)) {
      // console.log("session start");
      const date = new Date();
      const sessionObject = {
        _startTime: date,
        status: "_ONGOING",
        latitude: latitude,
        longitude: longitude
      };
      this.setState({
        currentSessionDetails: sessionObject,
        currentSessionStatus: "start",
        sessionStartime: new Date()
      });
      AsyncStorage.setItem("SESSION_START_TIME", date.toString());
      AsyncStorage.setItem("SESSION_STATUS", "start");
      AsyncStorage.setItem("SESSION_DETAILS", JSON.stringify(sessionObject));
    } else {
      if (this.state.currentSessionDetails) {
        this.showToast("Your session is start already.please complete it.");
        // ToastAndroid.show(
        //   "Your session is start already.please complete it.",
        //   ToastAndroid.LONG
        // );
        return;
      }
    }
  }

  stopSession() {
    // const { latitude, longitude } = this.state;
    // const distanceFromGym = this.getDistance();
    if (!this.isOngoingSession()) {
      this.showToast("You haven't started any session.");
      // ToastAndroid.show("You haven't started any session.", ToastAndroid.LONG);
      return;
    }

    const distanceFromGym = this.getDistance();

    if (distanceFromGym && distanceFromGym > fixedMeter) {
      this.showToast(
        "This session will not be considered as valid. Please Check Out at the gym next time"
      );
      // ToastAndroid.show(
      //   "This session will not be considered as valid. Please Check Out at the gym next time",
      //   ToastAndroid.LONG
      // );
      this.onLostSession();
      return;
    }
    const { currentSessionDetails } = this.state;
    const sessionTime = this.getSessionTime(currentSessionDetails._startTime);
    console.log("sessionTime => sessionTime", 45 - Math.round(sessionTime));
    const timeDiff = 45 - Math.round(sessionTime);
    Alert.alert(
      "INFIT",
      "You have " +
        timeDiff.toString() +
        " minutes left to complete a paid session with Infit. Are you sure you want to end the session? You will not be able to start a new session today",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => this.compeleteSession() }
      ],
      { cancelable: false }
    );

    // const date = new Date();
    // const calculatedTime = this.getMinitue(
    //   currentSessionDetails._startTime,
    //   date
    // );
    // // if (calculatedTime < 45) {
    // //   ToastAndroid.show(
    // //     "You can't stop Session before 45 minitue!",
    // //     ToastAndroid.LONG
    // //   );
    // //   return;
    // // }
    // currentSessionDetails._endTime = date;
    // currentSessionDetails.status = "COMPLETE";

    // this.setState({
    //   currentSessionDetails: null,
    //   currentSessionStatus: null,
    //   completeSession: currentSessionDetails
    // });

    // this.addInFirebase(currentSessionDetails);
    // this.getDataFromDb(user._id);
    // AsyncStorage.removeItem("SESSION_START_TIME");
    // AsyncStorage.removeItem("SESSION_STATUS");
    // AsyncStorage.removeItem("SESSION_DETAILS");
    // console.log("currentSessionDetails", currentSessionDetails);
  }

  onLostSession() {
    const { currentSessionDetails, user } = this.state;
    const date = new Date();
    currentSessionDetails._endTime = date;
    currentSessionDetails.status = "COMPLETE";

    this.setState({
      currentSessionDetails: null,
      currentSessionStatus: null,
      timeoutSession: null,
      completeSession: currentSessionDetails
    });

    this.getDataFromDb(user._id);
    AsyncStorage.removeItem("SESSION_START_TIME");
    AsyncStorage.removeItem("TIMEOUT_TIME");
    AsyncStorage.removeItem("SESSION_STATUS");
    AsyncStorage.removeItem("SESSION_DETAILS");
    console.log("currentSessionDetails", currentSessionDetails);
  }

  compeleteSession() {
    // const { latitude, longitude } = this.state;
    const { currentSessionDetails, user } = this.state;
    // const distanceFromGym = this.getDistance();
    const sessionTime = this.getSessionTime(currentSessionDetails._startTime);

    if (sessionTime > 44) {
      // ToastAndroid.show(
      //   "Awesome, you have finished your today’s session at your gym",
      //   ToastAndroid.LONG
      // );

      this.showToast(
        "Awesome, you have finished your today’s session at your gym"
      );
    }
    // const distanceFromGym = this.getDistance();

    // if (distanceFromGym && distanceFromGym > 100) {
    //   ToastAndroid.show(
    //     "You are not at gym. session will declined soon.",
    //     ToastAndroid.LONG
    //   );
    //   return;
    // }
    const date = new Date();
    // const calculatedTime = this.getMinitue(
    //   currentSessionDetails._startTime,
    //   date
    // );
    // if (calculatedTime < 45) {
    //   ToastAndroid.show(
    //     "You can't stop Session before 45 minitue!",
    //     ToastAndroid.LONG
    //   );
    //   return;
    // }
    currentSessionDetails._endTime = date;
    currentSessionDetails.status = "COMPLETE";

    this.setState({
      currentSessionDetails: null,
      currentSessionStatus: null,
      timeoutSession: null,
      completeSession: currentSessionDetails
    });

    this.addInFirebase(currentSessionDetails);
    this.getDataFromDb(user._id);
    AsyncStorage.removeItem("SESSION_START_TIME");
    AsyncStorage.removeItem("TIMEOUT_TIME");
    AsyncStorage.removeItem("SESSION_STATUS");
    AsyncStorage.removeItem("SESSION_DETAILS");
    console.log("currentSessionDetails", currentSessionDetails);
  }

  // async getSessionList() {
  //   const sessionList = await AsyncStorage.getItem("SESSION_LIST");
  //   // console.log("--ses", sessionList);
  //   const session = JSON.parse(sessionList);
  //   if (sessionList && session) {
  //     return session;
  //   }
  //   const temp = [];
  //   return temp;
  // }

  getMinitue(startTime, endTime) {
    // var diff = Math.abs(startTime - endTime);
    var seconds = (endTime.getTime() - new Date(startTime).getTime()) / 1000;
    var minitues = seconds / 60;
    return Math.abs(minitues);
  }

  onLogout() {
    AsyncStorage.removeItem("USERDETAILS");
    AsyncStorage.removeItem("SESSION_STATUS");
    AsyncStorage.removeItem("TIMEOUT_TIME");
    AsyncStorage.removeItem("SESSION_DETAILS");
    AsyncStorage.removeItem("SESSION_START_TIME");
    BackgroundGeolocation.removeAllListeners();
    Actions.LoginScreen();
  }

  // componentWillUnmount() {
  //   // unregister all event listeners
  //   BackgroundGeolocation.removeAllListeners();
  // }

  isOngoingSession() {
    if (
      this.state.currentSessionDetails &&
      this.state.currentSessionDetails._startTime
    ) {
      return true;
    }

    return false;
  }

  onRefresh() {
    const { user } = this.state;

    this.getDataFromDb(user._id);
  }

  render() {
    // console.log("this.state", this.state);
    // const { gymLatitude, gymLongitude, latitude, longitude } = this.state;
    const distanceFromGym = this.getDistance();
    const SessionStart = this.onCheckStartSession(distanceFromGym);
    // const countTime = this.onCount();
    const { currentSessionTime, sessionList } = this.state;
    // console.log("distanceFromGym in render", distanceFromGym, SessionStart);
    // this.enableLocation();

    if (this.state.noInternet) {
      return <NoInternetScreen />;
    }

    return (
      <Container style={{ backgroundColor: "#000" }}>
        <Toast ref="toast" style={{ backgroundColor: "#626262" }} />

        <Headers
          user={this.state.user}
          onLogout={() => this.onLogout()}
          onRefresh={() => this.onRefresh()}
        />
        <Tabs
          tabStyle={{ backgroundColor: "black" }}
          activeTabStyle={{ backgroundColor: "black" }}
          tabBarUnderlineStyle={{
            backgroundColor: "#FE4E1A",
            height: 2
          }}
        >
          <Tab
            heading="CURRENT SESSION"
            activeTextStyle={{
              color: "red",
              fontSize: 16,
              fontFamily: "TTNorms-Bold"
            }}
            tabStyle={{ backgroundColor: "black" }}
            activeTabStyle={{
              backgroundColor: "black"
            }}
            textStyle={{
              color: "#9B9B9B",
              fontSize: 16,
              fontFamily: "TTNorms-Bold"
            }}
          >
            <CurrentSessionTab
              distanceFromGym={distanceFromGym}
              warning={this.state.warning}
              timeoutSession={this.state.timeoutSession}
              currentSessionDetails={this.state.currentSessionDetails}
              currentSessionTime={currentSessionTime}
              currentLocation={this.state.currentLocation}
              sessionListCount={this.state.sessionList}
            />
          </Tab>
          <Tab
            heading="SESSION HISTORY"
            style={{
              flex: 1,
              backgroundColor: "black",
              width: "100%"
            }}
            activeTextStyle={{
              color: "red",
              fontSize: 16,
              fontFamily: "TTNorms-Bold"
            }}
            textStyle={{
              color: "#9B9B9B",
              fontSize: 16,
              fontFamily: "TTNorms-Bold"
            }}
            tabStyle={{ backgroundColor: "black" }}
            activeTabStyle={{
              backgroundColor: "black"
            }}
          >
            <SessionList sessionList={this.state.sessionList} />
          </Tab>
        </Tabs>
        <Footer>
          <FooterTab style={{ backgroundColor: "red" }}>
            <View style={{ flex: 1, height: 56, flexDirection: "row" }}>
              <Button
                style={{ backgroundColor: "red", flex: 1, width: "100%" }}
                onPress={() => this.startSession()}
              >
                <Title
                  style={{
                    fontWeight: "800",
                    fontSize: 18,
                    fontFamily: "TTNorms-Bold",
                    color: this.isOngoingSession() ? "#EEE" : "black"
                  }}
                >
                  START SESSION
                </Title>
              </Button>
              <Button
                style={{
                  backgroundColor: "#C21807",
                  flex: 1,
                  width: "100%",
                  marginTop: -1
                }}
                onPress={() => this.stopSession()}
              >
                <Title
                  style={{
                    fontWeight: "800",
                    fontSize: 18,
                    fontFamily: "TTNorms-Bold",
                    color: this.isOngoingSession() ? "black" : '"#EEE"'
                  }}
                >
                  STOP SESSION
                </Title>
              </Button>
            </View>
          </FooterTab>
        </Footer>
      </Container>
    );
  }

  // covertTime(d) {
  //   return moment(d).format(" DD/MM/YYYY");
  // }

  // covertstartTime(d) {
  //   return moment(d).format("h : m A");
  // }

  addInFirebase(sessionObject) {
    const { user } = this.state;
    console.log("user", user);
    // const firebaseObj = {
    //   ...sessionObject,
    //   user: {
    //     first_name: user.first_name,
    //     last_name: user.last_name,
    //     _id: user._id,
    //     mobile: user.mobile,
    //     lat_lon: user.lat_lon,
    //     token: user.token
    //   }
    // };

    const input = {
      user_id: user._id,
      start_time: sessionObject._startTime,
      end_time: sessionObject._endTime,
      latitude: sessionObject.latitude,
      longitude: sessionObject.longitude
    };

    fetch(URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        query: createTest,
        variables: {
          input
        }
      })
    })
      .then(response => {
        return response.json();
      })
      .then(response => {
        const user = get(
          response,
          "data.test_demo_location.database.create",
          null
        );

        // this.getDataFromDb(user._id);
        console.log("response", user);
        this.setState({ loading: false });
      });

    // this.setGymLocation();
    // return this.ref.add(firebaseObj);
  }

  getDataFromDb(id) {
    console.log("usususususussususuusussu", id);
    fetch(URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        query: getDataLocation,
        variables: {
          user_id: id
        }
      })
    })
      .then(response => {
        return response.json();
      })
      .then(response => {
        const user = get(response, "data.test_demo_location.database.all", []);
        this.setState({ sessionList: user.reverse() });
        // console.log("response", user);
        console.log("response from data", response);

        this.setState({ loading: false });
      });
  }
}
// {SessionStart ? null : distanceFromGym > 100 ? (
//   <View
//     style={{
//       alignItems: "center",
//       justifyContent: "center",
//       height: 56
//     }}
//   >
//     <Title style={{ fontWeight: "800", fontSize: 14, color: "red" }}>
//       you are {distanceFromGym} m away from here.
//     </Title>
//   </View>
// ) : currentSessionTime && currentSessionTime < 44 ? (
//   <View
//     style={{
//       alignItems: "center",
//       justifyContent: "center"
//     }}
//   >
//     <Title style={{ fontWeight: "800", fontSize: 14, color: "red" }}>
//       Your Current Session {this.state.isShowingText ? " : " : "   "}
//       {Math.round(currentSessionTime)} minitue
//     </Title>
//   </View>
// ) : null}

// <Right style={{ position: "absolute", top: 10, right: 20 }}>
// <Button
//   onPress={() => this.onLogout()}
//   style={{
//     elevation: 0,
//     backgroundColor: "transparent"
//   }}
// >
//   <View
//     style={{
//       marginTop: 30,
//       marginBottom: 20,
//       marginRight: 10,
//       height: 26,
//       width: 26,
//       alignSelf: "center"
//     }}
//   >
//     <Image
//       source={require("../images/power.png")}
//       style={{ height: "100%", width: "100%" }}
//     />
//   </View>
// </Button>
// </Right>

// <Right style={{ position: "absolute", top: 10, left: 20 }}>
// <Button
//   style={{
//     elevation: 0,
//     backgroundColor: "transparent"
//   }}
// >
//   <Text style={{ color: "red", fontSize: 14, fontWeight: "600" }}>
//     {this.state.user
//       ? this.state.user.first_name + " " + this.state.user.last_name
//       : "Hello There!"}
//   </Text>
// </Button>
// </Right>
// {SessionStart ? null : distanceFromGym > 100 ? (
//   <View
//     style={{
//       alignItems: "center",
//       justifyContent: "center",
//       height: 56
//     }}
//   >
//     <Title style={{ fontWeight: "800", fontSize: 14, color: "red" }}>
//       you are {distanceFromGym} m away from here.
//     </Title>
//   </View>
// ) : currentSessionTime && currentSessionTime < 44 ? (
//   <View
//     style={{
//       alignItems: "center",
//       justifyContent: "center"
//     }}
//   >
//     <Title style={{ fontWeight: "800", fontSize: 14, color: "red" }}>
//       Your Current Session {this.state.isShowingText ? " : " : "   "}
//       {Math.round(currentSessionTime)} minitue
//     </Title>
//   </View>
// ) : null}
