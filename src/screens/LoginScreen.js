import React, { Component } from "react";
import { View, Container } from "native-base";
// import { TextInput } from "react-native-paper";
import { Dimensions, Image, BackHandler } from "react-native";
import LoginSceenBox from "../components/LoginScreenComponents/LoginSceenBox";
var { height, width } = Dimensions.get("window");

class LoginScreen extends Component {
  state = {
    text: ""
  };

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    // this.goBack(); // works best when the goBack is async
    BackHandler.exitApp();
    return true;
  };

  render() {
    return (
      <Container>
        <View
          style={{
            height: height,
            width: width,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "#000"
          }}
        >
          <View
            style={{
              height: 70,
              width: 180,
              position: "absolute",
              top: 30,
              alignSelf: "center"
            }}
          >
            <Image
              source={require("../images/infit_1.png")}
              style={{ height: "100%", width: "100%" }}
            />
          </View>
          <LoginSceenBox />
        </View>
      </Container>
    );
  }
}
export default LoginScreen;
