import React, { Component } from "react";
import HomeScreen from "./screens/HomeScreen";
import LoginScreen from "./screens/LoginScreen";
import { Scene, Router, Stack, Actions } from "react-native-router-flux";
import {
  AsyncStorage,
  View,
  Text,
  Platform,
  ActivityIndicator,
  StyleSheet,
  DeviceEventEmitter,
  NetInfo
} from "react-native";
// import firebase from "react-native-firebase";
// import RNAndroidLocationEnabler from "react-native-android-location-enabler";
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import NoInternetScreen from "./components/CommonCompponent/NoInternetScreen";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      initial: false,
      noInternet: false
    };
  }

  checkAndEnableLocationService() {
    if (Platform.OS === "android") {
      LocationServicesDialogBox.checkLocationServicesIsEnabled({
        message:
          "<h2>Use Location ?</h2>Infit wants to change your device settings:<br/><br/>Use GPS, Wi-Fi, and cell network for location<br/><br/>",
        ok: "YES",
        cancel: "NO",
        enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
        showDialog: true, // false => Opens the Location access page directly
        openLocationServices: true, // false => Directly catch method is called if location services are turned off
        preventOutSideTouch: false, //true => To prevent the location services popup from closing when it is clicked outside
        preventBackClick: false, //true => To prevent the location services popup from closing when it is clicked back button
        providerListener: true // true ==> Trigger "locationProviderStatusChange" listener when the location state changes
      })
        .then(
          function(success) {
            console.log("ggg 1", success);
            if (!success.alreadyEnabled) {
              console.log("ggg 5", success);
              LocationServicesDialogBox.forceCloseDialog();
            }
          }.bind(this)
        )
        .catch(error => {
          console.log("ggg 3 ", error.message);
          this.checkAndEnableLocationService();
        });
    }
    return;
  }

  async componentDidMount() {
    const user = await AsyncStorage.getItem("USERDETAILS");
    try {
      // const user = await AsyncStorage.getItem("USERDETAILS");
      if (user) {
        console.log("user---->", JSON.parse(user));
        const userSetails = JSON.parse(user);
        if (userSetails && userSetails.mobile) {
          this.setState({
            initial: true,
            loading: false
          });
        }
      }
      this.setState({
        loading: false
      });
    } catch (err) {
      console.log("err ==> ", err);
      this.setState({
        loading: false
      });
    }

    // this.checkAndEnableLocationService();
    // DeviceEventEmitter.addListener("locationProviderStatusChange", status => {
    //   // only trigger when "providerListener" is enabled
    //   console.log("ggg", status); //  status => {enabled: false, status: "disabled"} or {enabled: true, status: "enabled"}
    //   if (status.enabled) {
    //     LocationServicesDialogBox.forceCloseDialog();
    //   }
    //   if (status.enabled == false) {
    //     this.checkAndEnableLocationService();
    //   }
    // });
    this.onCheckInternetConnetction();

    NetInfo.addEventListener(
      "connectionChange",
      handleFirstConnectivityChange => {
        if (handleFirstConnectivityChange.type === "none") {
          this.setState({
            noInternet: true
          });
        } else {
          console.log("hi i am no one", user);

          this.setState({
            noInternet: false
          });
        }
      }
    );
  }

  onCheckInternetConnetction() {
    NetInfo.getConnectionInfo().then(connectionInfo => {
      if (connectionInfo.type === "none") {
        this.setState({ noInternet: true });
      }
      console.log(
        "Initial, type: " +
          connectionInfo.type +
          ", effectiveType: " +
          connectionInfo.effectiveType
      );
    });
  }
  // componentWillUnmount() {
  //   NetInfo.removeEventListener(
  //     "connectionChange",
  //     handleFirstConnectivityChange
  //   );
  // }
  // componentWillUnmount() {
  //   // used only when "providerListener" is enabled
  //   LocationServicesDialogBox.stopListener(); // Stop the "locationProviderStatusChange" listener.
  // }
  render() {
    if (this.state.noInternet) {
      return <NoInternetScreen />;
    }

    if (this.state.loading) {
      return (
        <View
          style={[
            styles.container,
            {
              display: "flex",
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
              flexDirection: "column"
            }
          ]}
        >
          <ActivityIndicator size="large" color="red" />
          <Text style={{ color: "#fff", fontSize: 14 }}>loading..</Text>
        </View>
      );
    }
    return (
      <Router>
        <Stack key="root">
          <Scene
            key="LoginScreen"
            component={LoginScreen}
            hideNavBar
            initial={!this.state.initial}
          />
          <Scene
            key="HomeScreen"
            component={HomeScreen}
            hideNavBar
            initial={this.state.initial}
          />
        </Stack>
      </Router>
    );
  }
}

export default App;

var styles = StyleSheet.create({
  container: {
    backgroundColor: "#000"
  },
  header: {
    backgroundColor: "red"
  },
  title: {
    color: "#fedd1e"
  },
  footer: {
    backgroundColor: "#fedd1e",
    paddingLeft: 10,
    paddingRight: 10
  },
  footerBody: {
    justifyContent: "center",
    width: 200,
    flex: 1
  },
  icon: {
    color: "#fff"
  },
  map: {
    flex: 1
  },
  status: {
    fontSize: 12
  },
  markerIcon: {
    borderWidth: 1,
    borderColor: "#000000",
    backgroundColor: "rgba(0,179,253, 0.6)",
    width: 10,
    height: 10,
    borderRadius: 5
  }
});
