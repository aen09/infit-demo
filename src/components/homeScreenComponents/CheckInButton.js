import React, { Component } from "react";
import { View, Text, Button, Icon, Footer } from "native-base";
import { StyleSheet, Dimensions } from "react-native";
var { height, width } = Dimensions.get("window");

export default class CheckInButton extends Component {
  render() {
    return (
      <Footer
        style={{ backgroundColor: "transparent", elevation: 0, height: 70 }}
      >
        <Button
          style={[styles.buttonStyle, this.props.style]}
          onPress={() => this.props.onPress()}
        >
          <View style={styles.buttonView}>
            <Text style={styles.ButtonText}>
              {this.props.title || "Check In"}
            </Text>
            <View style={styles.IconContainer}>
              <Icon name="ios-arrow-forward" style={styles.iconStyle} />
            </View>
          </View>
        </Button>
      </Footer>
    );
  }
}
const styles = StyleSheet.create({
  buttonStyle: {
    height: 56,
    width: "60%",
    borderRadius: 28,
    backgroundColor: "#f90e50",
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center"
  },
  buttonView: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  iconStyle: {
    color: "#f90e50",
    fontSize: 32,
    alignSelf: "center",
    fontWeight: "900"
  },
  IconContainer: {
    height: 42,
    marginRight: 20,
    width: 42,
    borderRadius: 21,
    backgroundColor: "#fff",
    justifyContent: "center"
  },
  ButtonText: { color: "#fff", flex: 3, fontWeight: "700", textAlign: "center" }
});
