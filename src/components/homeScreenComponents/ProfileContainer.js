import React, { Component } from "react";
import { View, Text, Icon } from "native-base";
import { StyleSheet, Dimensions } from "react-native";
import { Avatar, Button } from "react-native-paper";
import { Title } from "react-native-paper";

var { height, width } = Dimensions.get("window");
export default class ProfileContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "Mr. Alvin",
      user: null
    };
  }

  onAdd() {
    if (this.props.distance === "Add Destination") {
      this.props.onRefreshPress();
    }
  }

  render() {
    return (
      <View style={styles.ProfileContainer}>
        <Icon name="ios-undo" style={styles.iconStyle} />
        <View style={styles.profileAvtarBorder}>
          <View style={styles.profileAvtar}>
            <Avatar.Image
              size={150}
              source={require("../../images/profile.jpg")}
            />
          </View>
        </View>
        <Title style={styles.titleText}>
          {this.props.user
            ? this.props.user.first_name + " " + this.props.user.last_name
            : this.props.name}
        </Title>
        <Text note style={{ color: "#fff" }} onPress={() => this.onAdd()}>
          {this.props.distance}
        </Text>
        {this.props.remaingTime === 0 ? null : (
          <Text
            note
            style={{ color: "#fff", marginTop: 10 }}
            onPress={() => this.onAdd()}
          >
            {this.props.remaingTime}
          </Text>
        )}

        <Icon
          name="ios-pin"
          style={styles.iconRight}
          onPress={() => this.props.onRefreshPress()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  bottom: {},
  ProfileContainer: {
    height: height / 2.3,
    borderBottomRightRadius: 35,
    borderBottomLeftRadius: 35,
    backgroundColor: "#fdc000",
    alignItems: "center",
    justifyContent: "center"
  },
  iconStyle: {
    color: "white",
    fontSize: 42,
    fontWeight: "900",
    position: "absolute",
    top: 50,
    left: 30
  },
  iconRight: {
    color: "white",
    fontSize: 32,
    fontWeight: "900",
    position: "absolute",
    top: 50,
    right: 30
  },
  profileAvtar: {
    height: 150,
    width: 150,
    overflow: "hidden",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 75,
    backgroundColor: "white"
  },
  profileAvtarBorder: {
    height: 151,
    width: 151,
    elevation: 25,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 75,
    backgroundColor: "#fdc000",
    marginTop: -20
  },
  titleText: {
    color: "#fff",
    fontSize: 18
  }
});
