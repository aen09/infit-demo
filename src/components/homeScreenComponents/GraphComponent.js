import React, { Component } from "react";
import { View, Text } from "native-base";
import { StyleSheet, Dimensions } from "react-native";
import moment from "moment";

import TimerCountdown from "react-native-timer-countdown";

var { height, width } = Dimensions.get("window");
export default class GraphComponent extends Component {
  getTime(timestamp) {
    if (timestamp) {
      const time = moment.duration(moment().diff(timestamp)).asMinutes;

      if (time) {
        return time;
      }
      return "Please Check In.";
    }
    return "Please Check In.";
  }
  render() {
    const time = this.props.time;
    console.log("time", time);

    return (
      <View style={styles.mainContainer}>
        <Text style={{ color: "red" }}>{this.props.time}</Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  mainContainer: {
    marginTop: -95,
    alignSelf: "center",
    elevation: 8,
    height: height / 5,
    overflow: "hidden",
    borderRadius: 25,
    width: "80%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#fff"
  }
});
