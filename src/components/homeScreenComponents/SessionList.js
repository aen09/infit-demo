import React, { Component } from "react";
import { View, Text, FlatList } from "react-native";
import moment from "moment";
import { Avatar, Card, Title } from "react-native-paper";

export default class SessionList extends Component {
  covertTime(d) {
    return moment(d).format(" DD/MM/YYYY");
  }

  covertstartTime(d) {
    return moment(d).format("hh : mm A");
  }

  totalSessionTime(item) {
    const date = new Date(item.start_time);

    let timeremaininig = date.getTime() - new Date(item.end_time).getTime();
    var minutes = Math.floor(timeremaininig / 60000);

    return Math.abs(minutes) + "min";
  }

  totalSessionTimeOnly(item) {
    const date = new Date(item.start_time);

    let timeremaininig = date.getTime() - new Date(item.end_time).getTime();
    var minutes = Math.floor(timeremaininig / 60000);

    return Math.abs(minutes);
  }

  onCount(list) {
    const result = list.filter(
      listItem => this.totalSessionTimeOnly(listItem) > 44
    );
    return result;
  }

  render() {
    console.log("list", this.props.sessionList);
    const NewList = this.onCount(this.props.sessionList);
    return (
      <View style={{ flex: 1, height: "100%" }}>
        {NewList && NewList.length > 0 ? (
          <View style={{ flex: 1 }}>
            <FlatList
              style={{ marginTop: 5, flex: 1 }}
              data={NewList}
              renderItem={({ item, index }) => {
                return (
                  <Card
                    style={{
                      width: "95%",
                      alignSelf: "center",
                      borderColor: "#1a1a1a",
                      borderWidth: 1,
                      marginVertical: 5,
                      padding: 10,
                      backgroundColor: "transparent"
                    }}
                  >
                    <View
                      style={{ flexDirection: "row", alignItems: "center" }}
                    >
                      <Avatar.Text
                        size={42}
                        label={index + 1}
                        color="#000"
                        style={{
                          backgroundColor: "#9b9b9b",
                          color: "#000",
                          fontSize: 12,
                          marginTop: 5
                        }}
                      />
                      <Title
                        style={{
                          fontSize: 14,
                          color: "#9b9b9b",
                          marginLeft: 15,
                          fontFamily: "TTNorms-Bold"
                        }}
                      >
                        {this.covertTime(item.start_time)}
                      </Title>

                      <Title
                        style={{
                          fontSize: 14,
                          color: "#9b9b9b",
                          marginLeft: 45,
                          fontFamily: "TTNorms-Bold"
                        }}
                      >
                        Session Time : {this.totalSessionTime(item)}
                      </Title>
                    </View>

                    <View
                      style={{
                        flexDirection: "row",
                        flex: 1,
                        marginLeft: 42,
                        paddingHorizontal: 22,
                        paddingVertical: 2,
                        width: "90%",
                        alignSelf: "center"
                      }}
                    >
                      <View style={{ flexDirection: "column", flex: 1 }}>
                        <Title
                          style={{
                            color: "#9b9b9b",
                            fontSize: 14,
                            fontFamily: "TTNorms-Bold"
                          }}
                        >
                          {this.covertstartTime(item.start_time)}
                        </Title>
                        <Text
                          style={{
                            color: "#9b9b9b",
                            fontSize: 12,
                            fontFamily: "TTNorms-Medium"
                          }}
                        >
                          Session Start
                        </Text>
                      </View>

                      <View style={{ flexDirection: "column", flex: 1 }}>
                        <Title
                          style={{
                            color: "#9b9b9b",
                            fontSize: 14,
                            fontFamily: "TTNorms-Bold"
                          }}
                        >
                          {this.covertstartTime(item.end_time)}
                        </Title>
                        <Text
                          style={{
                            color: "#9b9b9b",
                            fontSize: 12,
                            fontFamily: "TTNorms-Medium"
                          }}
                        >
                          Session end
                        </Text>
                      </View>
                    </View>
                  </Card>
                );
              }}
            />
          </View>
        ) : (
          <View
            style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
          >
            <Title
              style={{
                color: "white",
                width: "100%",
                textAlign: "center",
                fontFamily: "TTNorms-Bold"
              }}
            >
              No past Session
            </Title>
          </View>
        )}
      </View>
    );
  }
}
