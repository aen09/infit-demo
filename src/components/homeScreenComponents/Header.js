import React, { Component } from "react";
import { Header, Button, Left, Right, Body, Text, View } from "native-base";

import { Image } from "react-native";

export default class Headers extends Component {
  render() {
    return (
      <Header
        androidStatusBarColor={"black"}
        hasTabs
        style={{ backgroundColor: "black", height: 60 }}
      >
        <Left style={{ flex: 1.5, marginLeft: -20 }}>
          <Button
            style={{
              elevation: 0,
              backgroundColor: "transparent"
            }}
          >
            <Text
              style={{
                color: "red",
                fontSize: 14,
                marginLeft: 10,
                fontFamily: "TTNorms-Bold",
                fontWeight: "600"
              }}
            >
              {this.props.user
                ? this.props.user.first_name + " " + this.props.user.last_name
                : "Hello There!"}
            </Text>
          </Button>
        </Left>
        <Body style={{ flex: 2, marginLeft: -50 }}>
          <View
            style={{
              marginTop: 20,
              marginBottom: 20,
              height: 70,
              width: 150,
              alignSelf: "center",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Image
              source={require("../../images/logo.png")}
              style={{ height: "100%", width: "75%", resizeMode: "contain" }}
            />
          </View>
        </Body>

        <Right style={{ flex: 0.8 }}>
          <Button
            onPress={() => this.props.onRefresh()}
            style={{
              elevation: 0,
              marginRight: 10,
              backgroundColor: "transparent"
            }}
          >
            <View
              style={{
                height: 26,
                width: 26,
                alignSelf: "center"
              }}
            >
              <Image
                source={require("../../images/reload.png")}
                style={{ height: "100%", width: "80%", resizeMode: "contain" }}
              />
            </View>
          </Button>

          <Button
            onPress={() => this.props.onLogout()}
            style={{
              elevation: 0,
              backgroundColor: "transparent"
            }}
          >
            <View
              style={{
                height: 26,
                width: 26,
                alignSelf: "center"
              }}
            >
              <Image
                source={require("../../images/power.png")}
                style={{ height: "100%", width: "80%", resizeMode: "contain" }}
              />
            </View>
          </Button>
        </Right>
      </Header>
    );
  }
}
