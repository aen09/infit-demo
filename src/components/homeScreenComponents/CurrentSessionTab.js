import React, { Component } from "react";
import { View, Text } from "native-base";

export default class CurrentSessionTab extends Component {
  onCalculateTime(startDate) {
    console.log("show time and ", startDate);
    if (!startDate) {
      return "--:--";
    }
    const date = new Date();
    let timeremaininig =
      date.getTime() - new Date(startDate._startTime).getTime();

    console.log("date===>", date.getTime());
    console.log("date===>234", new Date(startDate._startTime).getTime());
    var minutes = Math.floor(timeremaininig / 60000);
    var seconds = ((timeremaininig % 60000) / 1000).toFixed(0);
    // var secDiff = timeremaininig / 1000; //in s
    // var minDiff = timeremaininig / 60 / 1000; //in minutes
    // var hDiff = timeremaininig / 3600 / 1000; //in hours
    // console.log("show time and minitue in react natie", timeremaininig);
    const showMinitues = minutes < 10 ? "0" + minutes : minutes;
    const showseconds = seconds < 10 ? "0" + seconds : seconds;
    return showMinitues + " : " + showseconds;
  }

  totalSessionTime(item) {
    const date = new Date(item.start_time);

    let timeremaininig = date.getTime() - new Date(item.end_time).getTime();
    var minutes = Math.floor(timeremaininig / 60000);

    return Math.abs(minutes);
  }

  onCount(list) {
    const result = list.filter(
      listItem => this.totalSessionTime(listItem) > 44
    );
    return result.length;
  }
  render() {
    console.log("this.props", this.props);
    const {
      currentSessionTime,
      currentSessionDetails,
      currentLocation
    } = this.props;
    const filterSessionList = this.onCount(this.props.sessionListCount);

    return (
      <View
        style={{
          alignItems: "center",
          backgroundColor: "black",
          justifyContent: "center",
          flex: 1
        }}
      >
        <View
          style={{
            alignItems: "center",
            width: "90%",
            paddingBottom: 30,
            backgroundColor: "#ffffff0f",
            justifyContent: "center"
          }}
        >
          <Text
            style={{
              color: "red",
              fontSize: 44,
              fontWeight: "900",
              marginVertical: 30,
              fontFamily: "TTNorms-Bold"
            }}
          >
            {this.props.currentSessionDetails &&
            currentSessionDetails._startTime
              ? this.onCalculateTime(currentSessionDetails)
              : "--:--"}
          </Text>
          <View
            style={{
              alignItems: "center",
              width: "90%",
              height: 2,
              marginVertical: 20,
              borderRadius: 10,
              backgroundColor: "#000",
              justifyContent: "center"
            }}
          />
          {this.props.timeoutSession ? (
            <Text
              style={{
                color: "red",
                fontSize: 14,
                marginVertical: 10,
                marginBottom: 20
              }}
            >
              your session will be decline after{" "}
              {this.props.timeoutSession
                ? 6 - Math.round(this.props.timeoutSession)
                : "00"}{" "}
              minitue
            </Text>
          ) : null}
          <View
            style={{
              width: "90%",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-around"
            }}
          >
            <View>
              <Text
                style={{
                  color: "red",
                  fontSize: 18,
                  fontWeight: "700",
                  fontFamily: "TTNorms-Bold",
                  textAlign: "center"
                }}
              >
                {this.props.currentLocation &&
                this.props.currentLocation.accuracy
                  ? Math.floor(this.props.currentLocation.accuracy)
                  : "00"}
                M
              </Text>
              <Text
                style={{
                  color: "#999",
                  fontSize: 14,
                  marginTop: 6,
                  fontFamily: "TTNorms-Medium",
                  textAlign: "center"
                }}
              >
                {"Location \nAccuracy"}
              </Text>
            </View>

            <View>
              <Text
                style={{
                  color: "red",
                  fontSize: 18,
                  fontWeight: "700",
                  fontFamily: "TTNorms-Bold",
                  textAlign: "center"
                }}
              >
                {this.props.distanceFromGym ? this.props.distanceFromGym : "00"}{" "}
                m
              </Text>
              <Text
                style={{
                  color: "#999",
                  fontSize: 14,
                  marginTop: 6,
                  fontFamily: "TTNorms-Medium",
                  textAlign: "center"
                }}
              >
                {"Distance \nfrom gym"}
              </Text>
            </View>
          </View>
        </View>

        <View
          style={{
            backgroundColor: "red",
            alignItems: "center",
            width: "90%",
            paddingVertical: 15,
            justifyContent: "center"
          }}
        >
          <Text
            style={{
              color: "#000",
              fontSize: 18,
              fontFamily: "TTNorms-Bold",
              textAlign: "center"
            }}
          >
            {filterSessionList < 9
              ? "0" + filterSessionList + " Session Completed"
              : filterSessionList + " Session Completed"}
          </Text>
        </View>
      </View>
    );
  }
}
// <Text style={{ color: "red", fontSize: 14, marginTop: 15 }}>
// {this.props.currentLocation && this.props.currentLocation.accuracy
//   ? "Location accuracy : " +
//     Math.floor(this.props.currentLocation.accuracy)
//   : "Open google map once"}
// </Text>

// <Text style={{ color: "red", fontSize: 14, marginTop: 5 }}>
// Distance from gym :{" "}
// {this.props.distanceFromGym ? this.props.distanceFromGym : "00"} m
// </Text>
