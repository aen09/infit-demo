import React, { Component } from "react";
import { Container, Content, View, Button, Text } from "native-base";
import { Image } from "react-native";

export default class NoInternetScreen extends Component {
  render() {
    return (
      <Container>
        <Content
          contentContainerStyle={{
            flex: 1,
            backgroundColor: "black",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <View style={{ height: 200, width: 200, backgroundColor: "black" }}>
            <Image
              source={require("../../images/noInternet.png")}
              style={{ height: "100%", width: "100%" }}
            />
          </View>
          <Text
            style={{
              alignSelf: "center",
              backgroundColor: "transparent",
              borderRadius: 4,
              color: "white",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            Check Your Internet Connection
          </Text>
        </Content>
      </Container>
    );
  }
}
