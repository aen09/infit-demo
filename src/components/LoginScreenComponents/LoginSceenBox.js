import React, { Component } from "react";
import { TextInput, AsyncStorage, Keyboard } from "react-native";
import { View, Text, Button, Title, Footer } from "native-base";
import OtpInputs from "react-native-otp-inputs";
import { get } from "lodash";
import { Actions } from "react-native-router-flux";
import Loader from "../basicStyleComponents/Loader.android";
import Toast from "react-native-easy-toast";

const URL = "https://api.infit.apps.actonatepanel.com/graphql";

const generateOtp = `mutation generateOtp($input :generateOTP_inputInputType) {
  otp_verifications{
    generateOTP(input : $input) {
      _id
      mobile
      code
      timestamp
      _deleted
      _created_at
      _updated_at
    }
  }
}`;

const verifyOtp = `
mutation VerifyLoginOtp($input : verifyLoginOTP_inputInputType) {
otp_verifications{
  verifyLoginOTP(input :$input) {
    _id
    firebase_token
    first_name
    gender
    last_name
    username
    password
    is_blogger
    address_line1
    address_line2
    email
    mobile
    lat_lon
    is_email_verified
    is_mobile_verified
    is_active
    last_active_at
    birthdate
    weight
    height
    wallet_balance
    _deleted
    token
    _created_at
    _updated_at
  }
}
}

`;

export default class LoginSceenBox extends Component {
  state = {
    mobile: "",
    code: null,
    newOtp: "",
    OtpModalStatus: false,
    LoginModalStatus: true,
    prefixNumber: "+91"
  };

  showToast(message) {
    this.refs.toast.show(message, 500, () => {
      // something you want to do at close
    });
  }

  onSignIn() {
    const { LoginModalStatus } = this.state;
    console.log("hi i am one clicked");

    if (LoginModalStatus) {
      if (this.state.mobile.length !== 10) {
        // ToastAndroid.show(
        //   "Please Enter Valid Mobile Number.",
        //   ToastAndroid.SHORT
        // );
        this.showToast("Please Enter Valid Mobile Number.");
        return;
      }
      this.setState({ loading: true }, () => {
        fetch(URL, {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            query: generateOtp,
            variables: {
              input: {
                phone: this.state.mobile
              }
            }
          })
        })
          .then(response => {
            return response.json();
          })
          .then(response => {
            console.log("data from fetch", response);
            const otp = get(
              response,
              "data.otp_verifications.generateOTP.code",
              null
            );
            console.log("otpotp", otp);

            this.setState({
              newOtp: otp,
              loading: false,
              OtpModalStatus: true,
              LoginModalStatus: false
            });
          })
          .catch(err => {
            this.setState({
              loading: false
            });
            console.log("err", err);
          });
      });
    } else {
      console.log("this.state", this.state);

      if (this.state.newOtp !== this.state.code) {
        this.showToast("Please Enter Correct OTP.");
        // ToastAndroid.show("Please Enter Correct OTP.", ToastAndroid.SHORT);
        return;
      }
      this.setState({ loading: true }, () => {
        fetch(URL, {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            query: verifyOtp,
            variables: {
              input: {
                phone: this.state.mobile,
                code: this.state.code,
                firebase_token: ""
              }
            }
          })
        })
          .then(response => {
            return response.json();
          })
          .then(response => {
            const user = get(
              response,
              "data.otp_verifications.verifyLoginOTP",
              null
            );
            AsyncStorage.setItem("USERDETAILS", JSON.stringify(user));
            console.log("user", user);
            if (user) {
              Actions.HomeScreen();
            }
            // console.log("data from fetch", response);
            // const otp = get(
            //   response,
            //   "data.otp_verifications.generateOTP.code",
            //   null
            // );
            this.setState({ loading: false });
            //
          })
          .catch(err => {
            console.log("err", err);
            this.setState({ loading: false });
          });
      });
    }
  }

  onResendOTP() {
    // this.setState({
    //   LoginModalStatus: true
    // });

    fetch(URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        query: generateOtp,
        variables: {
          input: {
            phone: this.state.mobile
          }
        }
      })
    })
      .then(response => {
        return response.json();
      })
      .then(response => {
        console.log("data from fetch", response);
        const otp = get(
          response,
          "data.otp_verifications.generateOTP.code",
          null
        );
        console.log("otpotp", otp);

        this.setState({
          newOtp: otp
        });
      });
  }

  onSetMobile(mobile) {
    if (mobile.length === 10) {
      Keyboard.dismiss();
    }
    this.setState({ mobile });
  }

  render() {
    const { OtpModalStatus } = this.state;
    return (
      <View
        style={{
          backgroundColor: "transparent",
          flex: 1,
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <Toast ref="toast" style={{ backgroundColor: "#626262" }} />
        {this.state.loading ? <Loader /> : null}
        <View
          style={{
            width: "90%",
            backgroundColor: "#ffffff0f",
            padding: 20,
            borderRadius: 8
          }}
        >
          <Title
            style={{
              color: "red",
              marginBottom: 10,
              fontWeight: "900",
              fontSize: 18,
              textAlign: "left"
            }}
          >
            Sign in
          </Title>
          <Text note style={{ color: "#999999" }}>
            {!OtpModalStatus
              ? "Enter your mobile number"
              : "Enter the verification code send to " + this.state.mobile}
          </Text>
          {!OtpModalStatus ? (
            <View
              style={{
                flexDirection: "row",
                marginVertical: 25
              }}
            >
              <TextInput
                placeholder="+91"
                returnKeyType="done"
                editable={false}
                placeholderTextColor="#000"
                keyboardType="number-pad"
                style={{
                  marginTop: 15,
                  width: 55,
                  textAlign: "center",
                  borderColor: "#ccc",
                  borderWidth: 1,
                  backgroundColor: "#fff",
                  borderRadius: 5
                }}
                value={this.state.prefixNumber}
              />
              <TextInput
                placeholder="Mobile Number"
                returnKeyType="done"
                placeholderTextColor="#ccc"
                maxLength={10}
                keyboardType="number-pad"
                style={{
                  marginTop: 15,
                  flex: 1,
                  paddingLeft: 15,
                  borderColor: "#ccc",
                  borderWidth: 1,
                  backgroundColor: "#fff",
                  marginLeft: 10,
                  borderRadius: 5,
                  padding: 10
                }}
                onChangeText={mobile => this.onSetMobile(mobile)}
                value={this.state.mobile}
              />
            </View>
          ) : (
            <View
              style={{
                flexDirection: "row",
                width: "50%",
                marginLeft: -25,
                alignItems: "flex-start"
              }}
            >
              <OtpInputs
                color="red"
                inputStyles={{
                  color: "red",
                  backgroundColor: "#fff",
                  borderColor: "#CCC",
                  borderWidth: 1,
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center",
                  alignSelf: "flex-start",
                  borderRadius: 5,
                  paddingTop: 0,
                  fontWeight: "800"
                }}
                style={{ backgroundColor: "red" }}
                focusedBorderColor="#74c222"
                inputContainerStyles={{
                  color: "#74c222",
                  marginLeft: 0,
                  backgroundColor: "#fff"
                }}
                handleChange={code => this.setState({ code })}
                numberOfInputs={4}
              />
            </View>
          )}
          <Text note style={{ marginTop: 15, fontSize: 12, marginBottom: 15 }}>
            We will send you verification code to your mobile number
          </Text>
        </View>

        <Footer
          style={{
            backgroundColor: "transparent",
            borderTopColor: "transparent",
            elevation: 0,
            position: "absolute",
            bottom: 40
          }}
        >
          <View
            style={{
              alignItems: "center",
              flexDirection: "row",
              width: "100%",
              justifyContent: "space-between"
            }}
          >
            {OtpModalStatus ? (
              <Button
                onPress={() => this.onResendOTP()}
                style={{
                  backgroundColor: "transparent",
                  alignSelf: "flex-start",
                  elevation: 0
                }}
              >
                <Title
                  style={{
                    color: "red",
                    fontWeight: "800",
                    fontSize: 16,
                    textAlign: "left"
                  }}
                >
                  Resend OTP
                </Title>
              </Button>
            ) : (
              <Title
                style={{
                  color: "transparent",
                  backgroundColor: "transparent",
                  fontWeight: "800",
                  fontSize: 16,
                  textAlign: "left"
                }}
              >
                .
              </Title>
            )}
            <Button
              onPress={() => this.onSignIn()}
              style={{
                alignSelf: "center",
                height: 52,
                borderRadius: 10,
                paddingHorizontal: 40,
                backgroundColor: "red"
              }}
            >
              <Title
                style={{
                  color: "#000",
                  fontWeight: "800",
                  fontSize: 16,
                  textAlign: "left"
                }}
              >
                {OtpModalStatus ? "Verify OTP" : "Sign In"}
              </Title>
            </Button>
          </View>
        </Footer>
      </View>
    );
  }
}
