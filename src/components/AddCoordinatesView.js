import React, { Component } from 'react';
import { TouchableOpacity, Modal, Text, View, TextInput } from 'react-native';
import { Button } from 'native-base';


export default class AddCoordinatorView extends Component {

  constructor(props) {
    super(props);

    this.state = {
      latitude: '',
      longitude: ''
    }
  }
  render() {
    const { latitude, longitude } = this.state;
    return (
      <Modal
        visible={this.props.visible}
        onRequestClose={() => this.props.close()}
        transparent
        >
        <View style={{ backgroundColor: '#00000069', flex:1, alignItems: 'center', justifyContent: 'center', padding: 20 }}>
          <View style={{ backgroundColor: '#fff', height: 250, width: 350, justifyContent: 'center', alignItems: 'center', borderRadius: 10 }}>
            <Text style={{ color: '#000', fontWeight: '600', fontSize: 16, marginBottom: 10 }}>Enter destination location</Text>
            <View>
              <TextInput keyboardType="numeric" style={{ borderColor: '#EEE', borderWidth: 1, width: 220, padding: 10 }} placeholder="Enter lattitude" onChangeText={(e) => this.setState({ latitude: e })}/>
              <TextInput keyboardType="numeric" style={{ borderColor: '#EEE', borderWidth: 1, width: 220, marginTop: 10, padding: 10 }} placeholder="Enter longitude" onChangeText={(e) => this.setState({ longitude: e })} />
            </View>
            <Button style={{ backgroundColor: 'yellow', borderRadius: 5, alignSelf: 'center', padding: 10, marginTop: 10  }} onPress={() => this.props.submit({ latitude, longitude })}>
              <Text style={{ color: '#000', fontWeight: '600', fontSize: 16 }}>Submit</Text>
            </Button>
          </View>
        </View>
      </Modal>
    )
  }
 }