/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { View, FlatList } from "react-native";

import moment from "moment";

import { Text } from "native-base";
import { Avatar, Button, Card, Title, Paragraph } from "react-native-paper";

export default class SessionList extends Component {
  render() {
    return (
      <FlatList
        data={this.props.data}
        style={{ marginTop: 10 }}
        keyExtractor={(item, index) => item.key}
        renderItem={({ item, index }) => (
          <Card
            style={{
              width: "95%",
              alignSelf: "center",
              marginVertical: 5,
              backgroundColor: "#ffffffbf"
            }}
          >
            <Card.Title
              titleStyle={{ fontSize: 16 }}
              title={item.date}
              subtitleStyle={{ marginTop: -5 }}
              subtitle={item.totalTime + " minitues"}
              left={props => (
                <Avatar.Text
                  size={42}
                  label={index + 1}
                  color="white"
                  style={{ backgroundColor: "#f90e50", color: "white" }}
                />
              )}
            />
            <Card.Content>
              <Paragraph>
                <Title style={{ fontSize: 14 }}>Session Start :</Title>
                {moment(item.startDateTime).format("HH:mm a")}
              </Paragraph>
              <Paragraph>
                <Title style={{ fontSize: 14, marginTop: -10 }}>
                  Session End :
                </Title>
                {moment(item.endDateTime).format("HH:mm a")}
              </Paragraph>
            </Card.Content>
          </Card>
        )}
      />
    );
  }
}
// <View
// style={{
//   display: "flex",
//   borderColor: item.status === "COMPLETED" ? "green" : "red",
//   borderWidth: 1,
//   borderRadius: 10,
//   height: 100,
//   width: "48%",
//   marginLeft: index % 2 === 0 ? 0 : "4%",
//   marginTop: 10
// }}
// >
// <Text style={{ color: "#fff", alignSelf: "center", marginTop: 5 }}>
//   {item.date}
// </Text>
// <View
//   style={{
//     display: "flex",
//     flexDirection: "row",
//     justifyContent: "space-around"
//   }}
// >
//   <Text style={{ color: "#fff", fontSize: 14 }}>Start Time</Text>
//   <Text style={{ color: "#fff", fontSize: 14 }}>End Time</Text>
// </View>
// <View
//   style={{
//     display: "flex",
//     flexDirection: "row",
//     justifyContent: "space-around"
//   }}
// >
//   <Text style={{ color: "#fff", fontSize: 14 }}>
//     {}
//   </Text>
//   <Text style={{ color: "#fff", fontSize: 14 }}>
//     {moment(item.endDateTime).format("HH:mm a")}
//   </Text>
// </View>
// <Text
//   style={{
//     color: "#fff",
//     fontSize: 14,
//     alignSelf: "center",
//     justifyContent: "flex-end"
//   }}
// >
//   Duration: {item.totalTime} minute(s)
// </Text>
// </View>
