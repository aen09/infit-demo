import React, { Component } from "react";
import { Avatar, Button, Card, Title, Paragraph } from "react-native-paper";

const LargeButton = () => (
  <Card
    style={{
      width: "90%",
      alignSelf: "center",
      marginTop: 20,
      elevation: 0,
      backgroundColor: "transparent"
    }}
  >
    <Card.Title
      style={{ backgroundColor: "transparent" }}
      title="Card Title"
      titleStyle={{
        fontSize: 13,
        color: "#CCC",
        fontWeight: "400",
        fontFamily: "Roboto-Medium"
      }}
      subtitleStyle={{
        fontSize: 15,
        color: "#626262",
        marginTop: -6,
        fontWeight: "400",
        fontFamily: "Roboto-Medium"
      }}
      subtitle="Card Subtitle"
      left={props => (
        <Avatar.Text
          size={32}
          color={"white"}
          style={{ backgroundColor: "#ccc", fontWeight: "800" }}
          label="1"
        />
      )}
    />
  </Card>
);

export default LargeButton;
