import React, { Component } from "react";
import { View, Text } from "native-base";
import { Appbar } from "react-native-paper";
import { StyleSheet } from "react-native";

export default class Header extends Component {
  render() {
    return (
      <View>
        <Appbar>
          <Appbar.Action
            icon="menu"
            onPress={() => console.log("Pressed menu")}
          />
          <Appbar.Header>
            <Text style={{ color: "white", fontWeight: "600" }}>INFIT</Text>
          </Appbar.Header>
        </Appbar>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  bottom: {}
});
