import React, { Component } from "react";
import {
  Avatar,
  Button,
  Card,
  Title,
  Paragraph,
  Colors
} from "react-native-paper";

const LargeButton = props => (
  <Card
    style={{
      backgroundColor: "#f90e50",
      marginTop: 20,
      height: 80,
      borderTopRightRadius: 40,
      elevation: 0
    }}
  >
    <Button
      icon="list"
      mode="contained"
      style={{
        elevation: 0,
        backgroundColor: "transparent",
        justifyContent: "center"
      }}
      contentStyle={{
        width: "100%",
        height: 100,
        marginTop: -8,
        alignSelf: "center",
        alignItems: "center"
      }}
      onPress={() => props.onPress()}
    >
      Session List
    </Button>
  </Card>
);

export default LargeButton;
