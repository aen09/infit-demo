import React, { Component } from "react";
import { ActivityIndicator, View, Text, StyleSheet } from "react-native";

export default class Loader extends Component {
  render() {
    return (
      <View
        style={[
          styles.container,
          {
            display: "flex",
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "column"
          }
        ]}
      >
        <ActivityIndicator
          size="large"
          color={this.props.errorLocation ? "red" : "#00ff00"}
        />
        <Text style={{ color: "#fff", fontSize: 14 }}>
          Getting your location...
        </Text>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    backgroundColor: "#f0f0f0"
  },
  header: {
    backgroundColor: "#fedd1e"
  },
  title: {
    color: "#fedd1e"
  },
  footer: {
    backgroundColor: "#fedd1e",
    paddingLeft: 10,
    paddingRight: 10
  },
  footerBody: {
    justifyContent: "center",
    width: 200,
    flex: 1
  },
  icon: {
    color: "#fff"
  },
  map: {
    flex: 1
  },
  status: {
    fontSize: 12
  },
  markerIcon: {
    borderWidth: 1,
    borderColor: "#000000",
    backgroundColor: "rgba(0,179,253, 0.6)",
    width: 10,
    height: 10,
    borderRadius: 5
  }
});
