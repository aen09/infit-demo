import React, { Component } from "react";
import { Modal, View, Text, ActivityIndicator, Dimensions } from "react-native";
import { Platform } from "react-native";
const Width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

export default class Loader extends Component {
  state = {
    modalVisible: true
  };
  render() {
    return (
      <Modal transparent visible={true}>
        <View
          style={{
            height: height,
            width: Width,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "transparent"
          }}
        >
          {Platform.OS === "ios" ? (
            <ActivityIndicator style={{ alignSelf: "center" }} color="red" />
          ) : (
            <ActivityIndicator
              style={{ alignSelf: "center" }}
              size={70}
              color="red"
            />
          )}
        </View>
      </Modal>
    );
  }
}
