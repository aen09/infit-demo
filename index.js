/**
 * @format
 */
import React, { Component } from "react";
import { AppRegistry, DeviceEventEmitter, Platform } from "react-native";
import App from "./src/index";
import { name as appName } from "./app.json";
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";

export default class MyApp extends Component {
  checkAndEnableLocationService() {
    if (Platform.OS === "android") {
      LocationServicesDialogBox.checkLocationServicesIsEnabled({
        message:
          "<h2>Use Location ?</h2>Infit wants to change your device settings:<br/><br/>Use GPS, Wi-Fi, and cell network for location<br/><br/>",
        ok: "YES",
        cancel: "NO",
        enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
        showDialog: false, // false => Opens the Location access page directly
        openLocationServices: true, // false => Directly catch method is called if location services are turned off
        preventOutSideTouch: false, //true => To prevent the location services popup from closing when it is clicked outside
        preventBackClick: true, //true => To prevent the location services popup from closing when it is clicked back button
        providerListener: true // true ==> Trigger "locationProviderStatusChange" listener when the location state changes
      })
        .then(
          function(success) {
            console.log("ggg 1", success);
            if (!success.alreadyEnabled) {
              console.log("ggg 5", success);
              LocationServicesDialogBox.forceCloseDialog();
            }
          }.bind(this)
        )
        .catch(error => {
          console.log("ggg 3 ", error.message);
          this.checkAndEnableLocationService();
        });
    }
    return;
  }

  componentDidMount() {
    this.checkAndEnableLocationService();
    DeviceEventEmitter.addListener("locationProviderStatusChange", status => {
      // only trigger when "providerListener" is enabled
      console.log("ggg", status); //  status => {enabled: false, status: "disabled"} or {enabled: true, status: "enabled"}
      if (status.enabled) {
        LocationServicesDialogBox.forceCloseDialog();
      }
      if (status.enabled == false) {
        this.checkAndEnableLocationService();
      }
    });
  }
  render() {
    return <App />;
  }
}

AppRegistry.registerComponent(appName, () => MyApp);
